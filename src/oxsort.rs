use std::sync::{Arc, Mutex};
use std::{thread, time};

pub fn oxidation_sort(val: Vec<u32>) -> Vec<u32> {
    let ret = Arc::new(Mutex::new(Vec::new()));
    let mut threads = Vec::new();

    for i in val {
        let list = ret.clone();

        threads.push(thread::spawn(move || {
            thread::sleep(time::Duration::from_millis(u64::from(i) * 2));

            list.lock().expect("Failed to lock the mutex").push(i);
        }));
    }

    for i in threads {
        i.join().expect("Cannot join thread");
    }

    return ret.lock().expect("Total fail").to_vec();
}
