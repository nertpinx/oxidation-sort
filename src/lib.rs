pub mod oxsort;

#[cfg(test)]
mod tests {
    use oxsort::oxidation_sort;

    #[test]
    fn basic_sort() {
        assert_eq!(oxidation_sort((1..6).collect()), [1, 2, 3, 4, 5]);
    }
    #[test]
    fn basic_reverse_sort() {
        assert_eq!(oxidation_sort((1..6).rev().collect()), [1, 2, 3, 4, 5]);
    }
    #[test]
    fn complex_sort() {
        let mut arr = vec![];
        arr.extend((1..6).rev());
        arr.extend(1..5);
        arr.extend((0..4).rev());
        assert_eq!(oxidation_sort(arr), [0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 5]);
    }
}
